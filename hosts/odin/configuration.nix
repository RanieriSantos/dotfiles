{
  imports = [
    ./hardware-configuration.nix
    ./disko.nix
  ];
  bespoke = {printing.enable = true;};
  hardware = {
    graphics = {
      enable = true;
      enable32Bit = true;
    };
  };
  networking = {
    hostName = "odin";
  };
  powerManagement.cpuFreqGovernor = "performance";
  console.keyMap = "br-abnt2";
}
