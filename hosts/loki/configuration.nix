{
  imports = [
    ./hardware-configuration.nix
  ];
  hardware = {
    graphics = {
      enable = true;
    };
  };
  networking = {
    hostName = "loki";
    wireless.iwd.enable = true;
  };
  services.illum.enable = true;
  console.keyMap = "br-abnt2";
}
