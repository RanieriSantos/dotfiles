{
  programs = {
    command-not-found.enable = false;
    nano.enable = false;
    fish = {
      enable = true;
      interactiveShellInit = "set fish_greeting";
    };
    dconf.enable = true;
    neovim = {
      enable = true;
      defaultEditor = true;
      viAlias = true;
      vimAlias = true;
    };
  };
}
