{
  services = {
    irqbalance.enable = true;
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };
    resolved = {
      enable = true;
      dnssec = "true";
      dnsovertls = "true";
      domains = [
        "~."
      ];
      fallbackDns = [
        "1.1.1.1"
        "1.0.0.1"
      ];
    };
    jitterentropy-rngd.enable = true;
    udisks2 = {
      enable = true;
      mountOnMedia = true;
    };
    zram-generator = {
      enable = true;
      settings = {
        zram0 = {
          zram-size = "ram / 2";
        };
      };
    };
    seatd.enable = true;
  };
}
