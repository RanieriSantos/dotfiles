{
  config,
  pkgs,
  ...
}: {
  users = {
    defaultUserShell = pkgs.fish;
    users.ranieri = {
      isNormalUser = true;
      uid = 1000;
      extraGroups =
        [
          "wheel"
          "gamemode"
        ]
        ++ (
          if config.virtualisation.libvirtd.enable
          then ["libvirtd"]
          else if config.services.seatd.enable
          then ["seat"]
          else []
        );
    };
  };
}
