{
  networking = {
    timeServers = ["pool.ntp.br"];
    dhcpcd.enable = false;
    nftables.enable = true;
    useNetworkd = true;
    firewall = {
      enable = true;
      allowedUDPPorts = [
        22000
        21027
      ];
      allowedTCPPorts = [22000];
    };
    stevenblack = {
      enable = true;
      block = [
        "fakenews"
        "gambling"
        "porn"
        "social"
      ];
    };
    nameservers = [
      "1.1.1.1"
      "1.0.0.1"
    ];
  };
}
