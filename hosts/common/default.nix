{
  imports = [
    ./boot
    ./nix
    ./programs
    ./services
    ./users
    ./networking
    ../../modules
  ];
  environment = {
    etc = {
      "profile" = {
        text = "umask 027";
      };
      "bashrc" = {
        text = "umask 027";
      };
    };
  };

  documentation.nixos.enable = false;

  time.timeZone = "America/Fortaleza";

  security = {
    rtkit.enable = true;
    auditd.enable = true;
    polkit.enable = true;
    pam = {
      services = {
        swaylock = {};
      };
    };
    sudo.wheelNeedsPassword = false;
  };

  systemd = {
    network.enable = true;
  };

  fonts = {
    enableDefaultPackages = true;
  };

  i18n = {
    defaultLocale = "pt_BR.UTF-8";
    extraLocaleSettings = {
      LC_CTYPE = "pt_BR.UTF-8";
      LC_COLLATE = "pt_BR.UTF-8";
      LC_ADDRESS = "pt_BR.UTF-8";
      LC_IDENTIFICATION = "pt_BR.UTF-8";
      LC_MEASUREMENT = "pt_BR.UTF-8";
      LC_MONETARY = "pt_BR.UTF-8";
      LC_NAME = "pt_BR.UTF-8";
      LC_NUMERIC = "pt_BR.UTF-8";
      LC_PAPER = "pt_BR.UTF-8";
      LC_TELEPHONE = "pt_BR.UTF-8";
      LC_TIME = "pt_BR.UTF-8";
    };
  };
}
