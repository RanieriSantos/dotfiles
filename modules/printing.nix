{
  lib,
  pkgs,
  config,
  ...
}: {
  options.bespoke.printing = {
    enable = lib.mkEnableOption {
      type = lib.types.bool;
      default = false;
      example = true;
      description = "Enable CUPS with gutenprint and mdns auto-discovery";
    };
  };
  config = lib.mkIf config.bespoke.printing.enable {
    services = {
      printing = {
        enable = true;
        drivers = [pkgs.gutenprint];
      };
      avahi = {
        enable = true;
        nssmdns4 = true;
      };
    };
  };
}
