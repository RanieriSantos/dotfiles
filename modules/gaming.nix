{
  lib,
  pkgs,
  config,
  ...
}: {
  options.gaming = {
    enable = lib.mkEnableOption {
      type = lib.types.bool;
      default = false;
      example = true;
      description = "Enable gaming module(steam, heroic, mangohud, gamemode, discord, proton-ge and gamescope)";
    };
  };
  config = lib.mkIf config.gaming.enable {
    environment.systemPackages = with pkgs; [
      heroic
      mangohud
      discord
    ];
    programs = {
      gamemode.enable = true;
      gamescope.enable = true;
      steam = {
        enable = true;
        extraCompatPackages = with pkgs; [proton-ge-bin];
      };
    };
  };
}
