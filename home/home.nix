{
  pkgs,
  inputs,
  config,
  lib,
  ...
}: {
  imports = [
    inputs.stylix.homeManagerModules.stylix
    inputs.nvf.homeManagerModules.default
    ./programs
    ./services
  ];
  home = {
    username = "ranieri";
    homeDirectory = "/home/ranieri";
    stateVersion = "24.05"; # Do not touch it!
    sessionVariables = {
      SSH_AUTH_SOCK = "/run/user/1000/gnupg/S.gpg-agent.ssh";
      MANPAGER = "nvim +Man!";
      GPG_TTY = "$(tty)";
    };
    shellAliases = {
      l = "ls -lh --color";
      la = "ls -lah --color";
      ".." = "cd ..";
      "..." = "cd ../..";
      free = "free -wh --si";
      hm = "home-manager switch --flake $HOME/.dotfiles/.";
      ns = "sudo nixos-rebuild switch --flake $HOME/.dotfiles/.";
    };
    packages = with pkgs; [
      keepassxc
      ouch
      grim
      slurp
      xdg-utils
    ];
  };
  nixpkgs = {
    config = {
      allowUnfree = true;
      allowUnfreePredicate = _: true;
    };
  };
  gtk.iconTheme = {
    name = "Papirus-Dark";
    package = pkgs.papirus-icon-theme;
  };
  nix = {
    package = pkgs.nix;
    settings = {
      extra-experimental-features = [
        "nix-command"
        "flakes"
      ];
    };
  };
  xdg = {
    userDirs = {
      enable = true;
      createDirectories = true;
      documents = "${config.home.homeDirectory}/Documentos";
      music = "${config.home.homeDirectory}/Música";
      publicShare = "${config.home.homeDirectory}/Público";
      videos = "${config.home.homeDirectory}/Vídeos";
      pictures = "${config.home.homeDirectory}/Imagens";
      desktop = null;
      templates = null;
    };
    portal = {
      enable = true;
      extraPortals = with pkgs; [
        kdePackages.xdg-desktop-portal-kde
        xdg-desktop-portal-hyprland
      ];
      config = {
        common = {
          default = [
            "kde"
          ];
          "org.freedesktop.impl.portal.ScreenCast" = ["hyprland"];
        };
      };
    };
    mimeApps = {
      enable = true;
      defaultApplications = {
        "application/pdf" = ["zathura.desktop"];
      };
    };
  };
  programs = {
    fish = {
      enable = true;
      functions = let
        curl = "${pkgs.curl}/bin/curl -s";
      in {
        qr-encode = "${curl} qrenco.de/$argv";
        wttr = "${curl} wttr.in/$argv";
        dict = "${curl} dict.org/d:$argv | ${pkgs.bat}/bin/bat";
      };
    };
    foot.enable = true;
    cmus.enable = true;
    fuzzel = {
      enable = true;
      settings = {
        main = {
          dpi-aware = true;
          icon-theme = "Papirus-Dark";
        };
      };
    };
    zathura.enable = true;
    starship = {
      enable = true;
      settings = {
        add_newline = false;
        cmd_duration = {
          disabled = true;
        };
        line_break = {
          disabled = true;
        };
        character = {
          error_symbol = "[➤](bold red)";
          success_symbol = "[➤](bold green)";
        };
      };
    };
    bash.enable = true;
    home-manager.enable = true;
    fastfetch.enable = true;
    btop = {
      enable = true;
      settings = {
        base_10_sizes = true;
        proc_sorting = "memory";
        update_ms = 1000;
      };
      package = pkgs.btop-rocm;
    };
    git = {
      enable = true;
      package = pkgs.gitMinimal;
      userName = "Ranieri de Oliveira Santos";
      userEmail = "4733578-RanieriSantos@users.noreply.gitlab.com";
      aliases = {
        s = "status";
        ps = "push";
        pl = "pull";
        d = "diff";
      };
    };
    mpv = {
      enable = true;
      scripts = with pkgs; [
        mpvScripts.thumbfast
        mpvScripts.sponsorblock
        mpvScripts.uosc
      ];
    };
    yazi.enable = true;
  };
  wayland = {
    windowManager = {
      sway = {
        enable = true;
        config = {
          defaultWorkspace = "workspace number 1";
          bars = [];
          modifier = "Mod4";
          window.titlebar = false;
          terminal = "foot";
          menu = "fuzzel";
          input = {
            "11912:45834:HUSKY_GAMING_Teclado_Gamer_Husky_Blizzard_Keyboard" = {
              xkb_layout = "br";
              xkb_variant = "abnt2";
            };
          };
          keybindings = let
            modifier = config.wayland.windowManager.sway.config.modifier;
            wpctl = "exec ${pkgs.wireplumber}/bin/wpctl";
            playerctl = "exec ${pkgs.playerctl}/bin/playerctl";
            helper = "Ctrl+${modifier}+Alt";
          in
            lib.mkOptionDefault {
              "XF86AudioRaiseVolume" = "${wpctl} set-volume @DEFAULT_SINK@ 5%+ -l 1";
              "XF86AudioLowerVolume " = "${wpctl} set-volume @DEFAULT_SINK@ 5%-";
              "XF86AudioPlay" = "${playerctl} play-pause";
              "XF86AudioPause" = "${playerctl} play-pause";
              "XF86AudioNext" = "${playerctl} next";
              "XF86AudioPrev" = "${playerctl} previous";
              "${helper}+m" = "${wpctl} set-mute @DEFAULT_SOURCE@ toggle";

              "${modifier}+p" = "exec ${config.wayland.windowManager.sway.config.menu}";
              "${modifier}+l" = "exec ${pkgs.swaylock}/bin/swaylock";

              "${helper}+i" = "exec ${pkgs.psmisc}/bin/killall ${pkgs.wlinhibit}/bin/wlinhibit || ${pkgs.wlinhibit}/bin/wlinhibit";
              "${helper}+h" = "exec ${pkgs.libnotify}/bin/notify-send $(date)";
            };
        };
        wrapperFeatures.gtk = true;
      };
    };
  };
}
