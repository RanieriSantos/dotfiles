{
  pkgs,
  config,
  ...
}: {
  imports = [];
  services = {
    syncthing = {
      enable = true;
      settings = {
        devices = {
          S10e = {
            id = "NQISG2V-52VPSIZ-KEFLLSJ-FB4LAUX-2AKTIFE-QSCS2PS-OG2HIUH-672PNQ4";
            name = "Ranieri's S10e";
          };
        };
        folders = {
          vault = {
            id = "vault";
            label = "Vault";
            path = "~/Vault";
            type = "sendonly";
            devices = ["S10e"];
          };
          public = {
            id = "public";
            label = "Public";
            path = "${config.xdg.userDirs.publicShare}";
            type = "sendreceive";
            devices = ["S10e"];
          };
        };
      };
    };
    udiskie.enable = true;
    mako.enable = true;
    gpg-agent = {
      enable = true;
      enableSshSupport = true;
      enableScDaemon = false;
      pinentryPackage = pkgs.pinentry-qt;
      sshKeys = ["4EDA933BE4E811ED8A76E86EBB662586A55CBC80"];
    };
    wlsunset = {
      enable = true;
      sunrise = "06:00";
      sunset = "18:00";
      temperature.night = 3500;
    };
    swayidle = {
      enable = true;
    };
  };
}
