{
  programs.tmux = {
    enable = true;
    mouse = true;
    prefix = "C-x";
    baseIndex = 1;
    extraConfig = "set-option -g default-terminal \"tmux-256color\"\n
    set-option -g terminal-features \"alacritty:rgb\"";
  };
}
