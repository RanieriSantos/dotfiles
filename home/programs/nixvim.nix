{
  programs.nvf = {
    enable = true;
    settings = {
      vim = {
        viAlias = true;
        vimAlias = true;
        autocomplete = {nvim-cmp = {enable = true;};};
        lsp = {
          enable = true;
          formatOnSave = true;
        };
        syntaxHighlighting = true;
        languages = {
          nix = {
            enable = true;
            extraDiagnostics.enable = true;
            format.enable = true;
            lsp.enable = true;
          };
          ts = {
            enable = true;
            format = {
              enable = true;
            };
            lsp.enable = true;
            treesitter.enable = true;
          };
        };
        theme = {
          enable = true;
          name = "catppuccin";
          style = "mocha";
        };
        git.enable = true;
        dashboard.dashboard-nvim = {enable = true;};
      };
    };
  };
}
