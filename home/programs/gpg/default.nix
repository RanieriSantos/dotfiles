{
  programs.gpg = {
    enable = true;
    settings = {
      keyid-format = "LONG";
    };
  };
}
