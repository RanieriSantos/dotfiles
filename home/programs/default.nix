{
  imports = [
    ./librewolf.nix
    ./gpg
    ./qutebrowser
    ./tmux.nix
    ./nixvim.nix
    ./stylix.nix
  ];
  programs.swaylock.enable = true;
}
