{
  description = "My flake";
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    stylix.url = "github:danth/stylix";
    nvf.url = "github:notashelf/nvf";
  };
  outputs = {
    nixpkgs,
    home-manager,
    disko,
    ...
  } @ inputs: let
    inherit (nixpkgs) lib;
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
  in {
    nixosConfigurations = {
      odin = lib.nixosSystem {
        inherit system;
        modules = [
          disko.nixosModules.disko
          ./hosts/odin/configuration.nix
          ./hosts/common
        ];
      };
      loki = lib.nixosSystem {
        inherit system;
        modules = [
          disko.nixosModules.disko
          ./modules/disko.nix
          ./hosts/loki/configuration.nix
          ./hosts/common
        ];
      };
    };
    homeConfigurations = {
      ranieri = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        extraSpecialArgs = {inherit inputs;};
        modules = [
          ./home/home.nix
        ];
      };
    };
  };
}
